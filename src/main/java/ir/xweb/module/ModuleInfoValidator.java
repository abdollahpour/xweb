package ir.xweb.module;

import java.util.List;


public interface ModuleInfoValidator {

    String getParam();

    String getRegex();

    boolean isRequire();

}

